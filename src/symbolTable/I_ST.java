package symbolTable;

public interface I_ST<Key, Value> {

	/**
	 * Returns the number of key-value pairs in this symbol table.
	 * @return the number of key-value pairs in this symbol table
	 */
	int size();

	/**
	 * Is this symbol table empty?
	 * @return <tt>true</tt> if this symbol table is empty and <tt>false</tt> otherwise
	 */
	boolean isEmpty();

	/**
	 * Does this symbol table contain the given key?
	 * @param key the key
	 * @return <tt>true</tt> if this symbol table contains <tt>key</tt> and
	 *     <tt>false</tt> otherwise
	 */
	boolean contains(Key key);

	/**
	 * Returns the value associated with the given key.
	 * @param key the key
	 * @return the value associated with the given key if the key is in the symbol table
	 *     and <tt>null</tt> if the key is not in the symbol table
	 */
	Value get(Key key);

	/**
	 * Inserts the key-value pair into the symbol table, overwriting the old value
	 * with the new value if the key is already in the symbol table.
	 * If the value is <tt>null</tt>, this effectively deletes the key from the symbol table.
	 * @param key the key
	 * @param val the value
	 */
	void put(Key key, Value val);

	/**
	 * Removes the key and associated value from the symbol table
	 * (if the key is in the symbol table).
	 * @param key the key
	 */
	void delete(Key key);

	/**
	 * Returns all keys in the symbol table as an <tt>Iterable</tt>.
	 * To iterate over all of the keys in the symbol table named <tt>st</tt>,
	 * use the foreach notation: <tt>for (Key key : st.keys())</tt>.
	 * @return all keys in the sybol table as an <tt>Iterable</tt>
	 */
	Iterable<Key> keys();

}